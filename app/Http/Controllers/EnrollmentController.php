<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\OrderDetail;
use DataTables;
use Storage;

class EnrollmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('enrollment.enrollment');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function datatable()
    {
        $enrollments = OrderDetail::with('user','laboratory','test_category.test')->select(['id', 'order_no','user_id','laboratory_id','test_id', 'amount', 'created_at']);
        return DataTables::of($enrollments)->make();
    }

}
