<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\Banner;
use Auth;
use Storage;
use Hash;
use DataTables;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function dashboard()
    {
        // return transaction();
        $data = [
            
            'data'              =>  15,
            'facility'          =>  10,
            'transaction'       =>  125,
            'transaction_amount'=>  187,
            'unread_message'    =>  156,
        ];

        return view('dashboard',$data);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function customers()
    {
        return view('customers.customers');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function customerDatatable()
    {
        $customers = WebUser::whereNull('deleted_at')->where('member',0)->select(['id','avatar','first_name','last_name','dob','address','email','licence','verified','created_at']);
        return DataTables::of($customers)->make();
    }

        /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function members()
    {
        return view('members.members');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function memberDatatable()
    {
        $members = WebUser::whereNull('deleted_at')->where('member',1)->select(['id','avatar','first_name','last_name','dob','address','email','total_avails','member_at','created_at','status']);
        return DataTables::of($members)->make();
    }

    /**
     * Update Status of specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function memberStatus(Request $request)
    {
        $response['status'] = false;
        $response['message'] = 'Oops! Something went wrong.';

        $id = $request->input('id');
        $status = $request->input('status');

        $item = WebUser::find($id);
        
        if ($item->update(['status' => $status])) {

            $response['status'] = true;
            $response['message'] = 'Status Updated Successfully.';
            return response()->json($response, 200);
        }
        return response()->json($response, 409);
    }

    /**
     * Update Status of specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function customerVerified(Request $request)
    {
        $response['status'] = false;
        $response['message'] = 'Oops! Something went wrong.';

        $id = $request->input('id');
        $status = $request->input('status');

        $item = WebUser::find($id);
        
        $item->verified_at = now();
        
        if ($item->update(['verified' => $status])) {

            $response['status'] = true;
            $response['message'] = 'Customer Verified successfully.';
            return response()->json($response, 200);
        }
        return response()->json($response, 409);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function settings()
    {
        $data = [
            'user' => Auth::user(),
        ];

        return view('settings', $data);
    }

    public function patch(Request $request)
    {
        $request->validate([
            'first_name'        => 'required|string|max:100',
            'last_name'         => 'required|string|max:100',
            'email'             => "required|max:191|unique:users,email,".Auth::user()->id,
            'current_password'  => 'required_if:change_password,1',
            'password'          => 'required_if:change_password,1|confirmed|min:6|max:22',
            'avatar'            => 'nullable|image|max:3000',
        ]);

        if($request->change_password == 1 && !Hash::check($request->current_password, Auth::user()->password))
        {
            return redirect()->back()->withErrors(['current_password'=>'your current password does not match'])->withInput($request->all());

        }

        $input_data = $request->except('password', 'password_confirmation','_token','_method','change_password','current_password');

        if($request->password)
        {
            $input_data['password'] = bcrypt($request->input('password'));
        }

        if($request->avatar)
        {
            Storage::disk('uploads')->delete(Auth::user()->avatar);
            $input_data['avatar'] = Storage::disk('uploads')->putFile('', $request->avatar);
        }

        Auth::user()->update($input_data);

        return redirect()->route('dashboard');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function bannerDelete($banner)
    {
        $banner = Banner::findOrFail($banner);
        // return $banner;
        Storage::disk('uploads')->delete($banner->banner);
        // apply your conditional check here
        $banner->delete();
        return redirect()->back();
    }
}
