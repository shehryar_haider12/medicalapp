<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Banner;
use DataTables;
use Storage;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('category.category');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function datatable()
    {
        $categories = Category::select(['id','image', 'name','status', 'created_at']);
        return DataTables::of($categories)->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'isEdit'        =>  false,
        ];
        return view('category.add-category',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name'              =>  'required|string|max:255',
            'image'             =>  'required|max:3000|mimes:png,jpg,jpeg,webp',

        ]);
        $data['image'] = Storage::disk('uploads')->putFile('',$request->image);
        $category = Category::create($data);
        if($request->banner)
        {
            foreach ($request->banner as $key => $banner) {
                $img = Storage::disk('uploads')->putFile('',$banner);
                $banner_data = [
                    'banner'        =>   $img,
                    'foreign_id'    =>   $category->id,
                    'type'          =>   'category',
                ];
                Banner::create($banner_data);
            }
        }
        return redirect()->route('category');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $data = [
            'isEdit'     =>  true,
            'category'   =>  $category
        ];
        return view('category.add-category',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $data = $request->validate([
            'name'      =>  'required|string|max:255',
            'image'     =>  'nullable|max:3000|mimes:png,jpg,jpeg,webp',
        ]);
        if($request->image)
        {
            Storage::disk('uploads')->delete($category->image);
            $data['image'] = Storage::disk('uploads')->putFile('',$request->image);
        }
        $category->update($data);

        if($request->banner)
        {
            foreach ($request->banner as $key => $banner) {
                $img = Storage::disk('uploads')->putFile('',$banner);
                $banner_data = [
                    'banner'        =>   $img,
                    'foreign_id'    =>   $category->id,
                    'type'          =>   'category',
                ];
                Banner::create($banner_data);
            }

        }
        return redirect()->route('category');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function status(Request $request)
    {
        $response['status'] = false;
        $response['message'] = 'Oops! Something went wrong.';

        $id     = $request->input('id');
        $status = $request->input('status');

        $item = Category::find($id);

        if ($item->update(['status' => $status])) {

            $response['status'] = true;
            $response['message'] = 'status updated successfully.';
            return response()->json($response, 200);
        }
        return response()->json($response, 409);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request)
    {
        $category = Category::findOrFail($request->id);
        // Storage::disk('uploads')->delete($category->image);
        // apply your conditional check here
        if ( $category->delete()) {
            $response['success'] = 'This Laboratory Deleted Successfully';
            return response()->json($response, 200);
        } else {
            $response['error'] = 'Oops Something went wrong!';
            return response()->json($response, 409);
        }
    }
}
