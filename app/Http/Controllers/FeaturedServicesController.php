<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\FeaturedServices;
use Storage;
use DataTables;

class FeaturedServicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('services.index');
    }

    public function datatable()
    {
        $services = FeaturedServices::all();
        return DataTables::of($services)->make();
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data=[
            'isEdit' => false
        ];
        return view('services.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name'          =>  'required|string|max:255',
            'image'        =>  'required|max:3000|mimes:png,jpg,jpeg,webp',
          ]);
          $data['image']      = Storage::disk('uploads')->putFile('',$request->image);
          FeaturedServices::create($data);
          return redirect()->route('services.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(FeaturedServices $services)
    {
        $data=[
            'isEdit' => true,
            'services' => $services
          ];
          return view('services.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FeaturedServices $services)
    {
        $data =   $request->validate([
            'name'          =>  'required|string|max:255',
            'image'        =>  'nullable|max:3000|mimes:png,jpg,jpeg,webp',
          ]);
          if($request->image)
          {
            Storage::disk('uploads')->delete($services->image);
            $data['image'] = Storage::disk('uploads')->putFile('',$request->image);
          }
          $services->update($data);
          return redirect()->route('services.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $services = FeaturedServices::findOrFail($request->id);
        // apply your conditional check here
        if ( $services->delete()) {
            $response['success'] = 'This Service Deleted Successfully';
            return response()->json($response, 200);
        } else {
            $response['error'] = 'Oops Something went wrong!';
            return response()->json($response, 409);
        }
    }
}
