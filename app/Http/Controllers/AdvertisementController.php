<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Advertisement;
use Storage;
use DataTables;

class AdvertisementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('advertisement.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function datatable()
    {
        $advertisement = Advertisement::select(['id','banner', 'name','status', 'created_at']);
        return DataTables::of($advertisement)->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data=[
          'isEdit' => false
        ];
        return view('advertisement.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $data = $request->validate([
          'name'          =>  'required|string|max:255',
          'description'   =>  'required',
          'banner'        =>  'required|max:3000|mimes:png,jpg,jpeg,webp',
        ]);
        $data['banner']      = Storage::disk('uploads')->putFile('',$request->banner);
        Advertisement::create($data);
        return redirect()->route('advertisement.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Advertisement $advertisement)
    {
        // dd($id);
        $data=[
          'isEdit' => true,
          'adv' => $advertisement
        ];
        return view('advertisement.create',$data);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Advertisement $advertisement)
    {
        $data =   $request->validate([
          'name'          =>  'required|string|max:255',
          'description'   =>  'required',
          'banner'        =>  'nullable|max:3000|mimes:png,jpg,jpeg,webp',
        ]);
        if($request->banner)
        {
          Storage::disk('uploads')->delete($advertisement->banner);
          $data['banner'] = Storage::disk('uploads')->putFile('',$request->banner);
        }
        $advertisement->update($data);
        return redirect()->route('advertisement.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function status(Request $request)
    {
        $response['status'] = false;
        $response['message'] = 'Oops! Something went wrong.';

        $id     = $request->input('id');
        $status = $request->input('status');
      
        $item = Advertisement::find($id);

        if ($item->update(['status' => $status])) {

            $response['status'] = true;
            $response['message'] = 'status updated successfully.';
            return response()->json($response, 200);
        }
        return response()->json($response, 409);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
      // dd($request->all());
      $adv = Advertisement::findOrFail($request->id);
        // apply your conditional check here
        if ( $adv->delete()) {
            $response['success'] = 'This Advertisement Deleted Successfully';
            return response()->json($response, 200);
        } else {
            $response['error'] = 'Oops Something went wrong!';
            return response()->json($response, 409);
        }
    }
}
