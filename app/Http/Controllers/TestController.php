<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Banner;
use App\Models\LaboratoryTest;
use App\Models\TestCategoryDetail;
use DataTables;
use Storage;

class TestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('test.test');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function datatable()
    {
        $tests = LaboratoryTest::select(['id','image', 'name','is_featured','status', 'created_at']);
        return DataTables::of($tests)->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'isEdit'        =>  false,
            'categories'    =>  Category::where('status',1)->get()
        ];
        return view('test.add-test',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name'              =>  'required|string|max:255',
            'image'             =>  'required|max:3000|mimes:png,jpg,jpeg,webp',
            'category_id'       =>  'required|array',
            'banner'            =>  'required|array',
            'description'       =>  'required',
        ]);
        
        $data['image']       = Storage::disk('uploads')->putFile('',$request->image);
        $data['is_featured'] = $request->is_featured ? 1 : 0;
    
        $test = LaboratoryTest::create($data);
        if($request->banner)
        {
            foreach ($request->banner as $key => $banner) {
                $img = Storage::disk('uploads')->putFile('',$banner);
                $banner_data = [
                    'banner'        =>   $img,
                    'foreign_id'    =>   $test->id,
                    'type'          =>   'test',
                ];
                Banner::create($banner_data);
            }
        }
        if($request->category_id)
        {
            foreach ($request->category_id as $key => $category) {
                $category_data = [
                    'category_id'   =>   $category,
                    'test_id'       =>   $test->id,
                ];
                TestCategoryDetail::create($category_data);
            }
        }
        return redirect()->route('test');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(LaboratoryTest $test)
    {
        // return $test->category;
        $data = [
            'isEdit'        =>  true,
            'categories'    =>  Category::where('status',1)->get(),
            'test'          =>  $test
        ];
        return view('test.add-test',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LaboratoryTest $test)
    {
        $data = $request->validate([
            'name'              =>  'required|string|max:255',
            'image'             =>  'nullable|max:3000|mimes:png,jpg,jpeg,webp',
            'category_id'       =>  'required|array',
            'banner'            =>  'nullable|array',
            'description'       =>  'required',
        ]);
        if($request->image)
        {
            Storage::disk('uploads')->delete($test->image);
            $data['image'] = Storage::disk('uploads')->putFile('',$request->image);
        }
        $data['is_featured'] = $request->is_featured ? 1 : 0;
        $test->update($data);

        if($request->banner)
        {
            $banners = Banner::where([['foreign_id',$test->id],['type','test']])->get();
            foreach ($banners as $key => $banner) {
                Storage::disk('uploads')->delete($banner->banner);
                $banner->delete();
            }
            foreach ($request->banner as $key => $banner) {
                $img = Storage::disk('uploads')->putFile('',$banner);
                $banner_data = [
                    'banner'        =>   $img,
                    'foreign_id'    =>   $test->id,
                    'type'          =>   'test',
                ];
                Banner::create($banner_data);
            }

        }
        if($request->category_id)
        {
            TestCategoryDetail::where([['test_id',$test->id]])->delete();
            foreach ($request->category_id as $key => $category) {
                $category_data = [
                    'category_id'   =>   $category,
                    'test_id'       =>   $test->id,
                ];
                TestCategoryDetail::create($category_data);
            }
        }
        return redirect()->route('test');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function status(Request $request)
    {
        $response['status'] = false;
        $response['message'] = 'Oops! Something went wrong.';

        $id     = $request->input('id');
        $status = $request->input('status');

        $item = LaboratoryTest::find($id);

        if ($item->update(['status' => $status])) {

            $response['status'] = true;
            $response['message'] = 'status updated successfully.';
            return response()->json($response, 200);
        }
        return response()->json($response, 409);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request)
    {
        $test = LaboratoryTest::findOrFail($request->id);
        // Storage::disk('uploads')->delete($test->image);
        // apply your conditional check here
        if ( $test->delete()) {
            $response['success'] = 'This Test Deleted Successfully';
            return response()->json($response, 200);
        } else {
            $response['error'] = 'Oops Something went wrong!';
            return response()->json($response, 409);
        }
    }
}
