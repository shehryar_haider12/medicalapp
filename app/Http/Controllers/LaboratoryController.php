<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Laboratory;
use App\Models\Banner;
use DataTables;
use Storage;

class LaboratoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('laboratory.laboratory');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function datatable()
    {
        $laboratory = Laboratory::select(['id','image', 'name','address','status', 'created_at']);
        return DataTables::of($laboratory)->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'isEdit'    =>  false,
        ];
        return view('laboratory.add-laboratory',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name'          =>  'required|string|max:255',
            'address'       =>  'required|string|max:255',
            'description'   =>  'required|string|max:255',
            'image'         =>  'required|max:3000|mimes:png,jpg,jpeg,webp',
        ]);
        $data['image'] = Storage::disk('uploads')->putFile('',$request->image);
        $laboratory = Laboratory::create($data);
        if($request->banner)
        {
            foreach ($request->banner as $key => $banner) {
                $img = Storage::disk('uploads')->putFile('',$banner);
                $banner_data = [
                    'banner'        =>   $img,
                    'foreign_id'    =>   $laboratory->id,
                    'type'          =>   'laboratory',
                ];
                Banner::create($banner_data);
            }
        }
        return redirect()->route('laboratory');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Laboratory $laboratory)
    {
        $data = [
            'isEdit'        =>  true,
            'laboratory'    =>  $laboratory
        ];
        
        return view('laboratory.add-laboratory',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Laboratory $laboratory)
    {
        $data = $request->validate([
            'name'      =>  'required|string|max:255',
            'address'   =>  'required|string|max:255',
            'description'   =>  'required|string|max:255',
            'image'     =>  'nullable|max:3000|mimes:png,jpg,jpeg,webp',
        ]);
        if($request->image)
        {
            Storage::disk('uploads')->delete($laboratory->image);
            $data['image'] = Storage::disk('uploads')->putFile('',$request->image);
        }
        $laboratory->update($data);

        if($request->banner)
            {
            foreach ($request->banner as $key => $banner) {
                $img = Storage::disk('uploads')->putFile('',$banner);
                $banner_data = [
                    'banner'        =>   $img,
                    'foreign_id'    =>   $laboratory->id,
                    'type'          =>   'laboratory',
                ];
                Banner::create($banner_data);
            }
        }
        return redirect()->route('laboratory');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function status(Request $request)
    {
        $response['status'] = false;
        $response['message'] = 'Oops! Something went wrong.';

        $id     = $request->input('id');
        $status = $request->input('status');

        $item = Laboratory::find($id);

        if ($item->update(['status' => $status])) {

            $response['status'] = true;
            $response['message'] = 'status updated successfully.';
            return response()->json($response, 200);
        }
        return response()->json($response, 409);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request)
    {
        $laboratory = Laboratory::findOrFail($request->id);
        // apply your conditional check here
        if ( $laboratory->delete()) {
            $response['success'] = 'This Laboratory Deleted Successfully';
            return response()->json($response, 200);
        } else {
            $response['error'] = 'Oops Something went wrong!';
            return response()->json($response, 409);
        }
    }
}
