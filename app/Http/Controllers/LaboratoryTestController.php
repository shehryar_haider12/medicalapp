<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\LaboratoryTest;
use App\Models\Laboratory;
use App\Models\TestCategory;
use App\Models\Category;
use App\Models\Banner;
use App\Models\IncludeTest;
use DataTables;
use Storage;

class LaboratoryTestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return "asdfasdf";
        return view('laboratory-test.laboratory-test');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function datatable()
    {
        // return "adfadf";
        $laboratory_test = TestCategory::with('test','laboratory')->select(['id','test_id','foreign_id','fee','recommend','report_in_days', 'created_at'])->where('type','laboratory')->get();
        return DataTables::of($laboratory_test)->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'isEdit'        =>  false,
            'laboratories'  =>  Laboratory::where('status',1)->get(),
            'tests'         =>  LaboratoryTest::get(),
        ];
        return view('laboratory-test.add-laboratory-test',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'laboratory_id'     =>  'required',
            'test_id'           =>  'required',
            'fee'               =>  'required|numeric',
            'report_in_days'    =>  'required|max:3',
            'recommend'         =>  'required',
        ]);
        $data = [
            'test_id'           =>    $request->test_id,
            'foreign_id'        =>    $request->laboratory_id,  
            'fee'               =>    $request->fee,
            'report_in_days'    =>    $request->report_in_days,
            'recommend'         =>    $request->recommend,
            'type'              =>    'laboratory'
        ];

        $test = TestCategory::create($data);

        if ($request->include_test_id) {
            foreach ($request->include_test_id as $include_test) {
                $data = [
                    'lab_test_id'   =>  $test->id,
                    'test_id'       =>  $include_test,
                ];
                IncludeTest::create($data);
            }
        }
        return redirect()->route('laboratory-test');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(TestCategory $laboratory_test)
    {
        $data = [
            'isEdit'            =>  true,
            'laboratories'      =>  Laboratory::where('status',1)->get(),
            'tests'             =>  LaboratoryTest::get(),
            'laboratory_test'   =>  $laboratory_test
        ];
        // return $laboratory_test->includeTest;
        return view('laboratory-test.add-laboratory-test',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TestCategory $laboratory_test)
    {
        $request->validate([
            'laboratory_id'     =>  'required',
            'test_id'           =>  'required',
            'fee'               =>  'required|numeric',
            'report_in_days'    =>  'required|max:3',
            'recommend'         =>  'required',
        ]);
        $data = [
            'test_id'           =>    $request->test_id,
            'foreign_id'        =>    $request->laboratory_id,  
            'fee'               =>    $request->fee,
            'report_in_days'    =>    $request->report_in_days,
            'recommend'         =>    $request->recommend,
            'type'              =>    'laboratory'
        ];
        
        $laboratory_test->update($data);
        
        return redirect()->route('laboratory-test');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function status(Request $request)
    {
        $response['status'] = false;
        $response['message'] = 'Oops! Something went wrong.';

        $id     = $request->input('id');
        $status = $request->input('status');

        $item = TestCategory::where('id',$id)->first();

        if ($item->update(['status' => $status])) {

            $response['status'] = true;
            $response['message'] = 'status updated successfully.';
            return response()->json($response, 200);
        }
        return response()->json($response, 409);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request)
    {
        $laboratory_test =TestCategory::find($request->id);
        // return $laboratory_test;
        // apply your conditional check here
        if ( $laboratory_test->delete()) {
            $response['success'] = 'This Laboratory Test Deleted Successfully';
            return response()->json($response, 200);
        } else {
            $response['error'] = 'Oops Something went wrong!';
            return response()->json($response, 409);
        }
    }
}
