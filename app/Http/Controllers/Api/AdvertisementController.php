<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Advertisement;

class AdvertisementController extends Controller
{
    public $successStatus = 200;
    public $errorStatus = 401;

    public function Banners()
    {
      $all=Advertisement::select(['id','banner','name'])
      ->orderBy('created_at','desc')
      ->get();
      return result($all, $this->successStatus, 'All Banners');
    }
}
