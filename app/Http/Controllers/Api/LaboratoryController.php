<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Laboratory;
use App\Models\LaboratoryTest;
use App\Models\TestCategory;

class LaboratoryController extends Controller
{
  public $successStatus = 200;
  public $errorStatus = 401;

  public function recentLabs()
  {
    $recent=Laboratory::orderBy('created_at','desc')
    ->take(5)
    ->select(['id','image','name'])
    ->get();

    return result($recent, $this->successStatus, 'success');
  }

  public function allLabs(Request $request)
  {
    $request->validate([
      'keyword' => 'nullable|string',
    ]);
    if(isset($request->keyword) && $request->keyword)
    {
      $all=Laboratory::select(['id','image','name'])
      ->where('name','LIKE','%'.$request->keyword.'%')
      ->orderBy('created_at','desc')
      ->get();  
    }
    else{
      $all=Laboratory::select(['id','image','name','address','description'])
      ->orderBy('created_at','desc')
      ->get();
    }
    return result($all, $this->successStatus, 'success');
  }

  public function labDetail(Request $request)
  {
    $request->validate([
      'laboratory_id' =>  'required|exists:laboratories,id',
      'keyword'       =>  'nullable'
    ]);
    
    $tests = Laboratory::with('testLaboratory.test','banners')->where([['id',$request->laboratory_id]])->first();
    // return $tests;
    $data['laboratory']['id']               = (int)$tests->id;
    $data['laboratory']['name']             = $tests->name ?? '';
    $data['laboratory']['image']            = $tests->image ?? '';
    $data['laboratory']['description']      = $tests->description ?? '';
    $data['laboratory']['average_rating']   = 4 ?? 0;

    $data['banners'] = [];
    $data['tests']   = [];

    if(isset($request->keyword) && $request->keyword)
    {
      $search_filter = LaboratoryTest::where('name','LIKE','%'.$request->keyword.'%')->get();
      $test_id       = $search_filter->pluck('id')->toArray();

      $test_laboratories=TestCategory::with('test')->orderBy('created_at','desc')->where([['foreign_id',$request->laboratory_id],['type','laboratory']])->whereIn('test_id',$test_id)->get();
    }else{

      $test_laboratories=TestCategory::with('test')->orderBy('created_at','desc')->where([['foreign_id',$request->laboratory_id],['type','laboratory']])->get();
    }

    foreach ($test_laboratories as $key => $test) {
      $data['tests'][$key]['id']           = (int)$test->test->id;
      $data['tests'][$key]['name']         = $test->test->name ?? '';
      $data['tests'][$key]['description']  = $test->test->description ?? '';
      $data['tests'][$key]['recommend']    = $test->test->recommend ?? '';
      $data['tests'][$key]['amount']       = (int)$test->test->fee ?? 0;
    }
    foreach ($tests->banners as $key => $banner) {
      $data['banners'][$key]['id']        = (int)$banner->id;
      $data['banners'][$key]['banner']    = $banner->banner ?? '';
    }
    return result($data, $this->successStatus, 'All Laboratory Tests');
  }

  public function testLaboratory(Request $request)
  {
    $request->validate([
      'laboratory_id' => 'required|exists:laboratories,id'
    ]);
    $test_laboratories=TestCategory::with('test')->orderBy('created_at','desc')
    ->where([['foreign_id',$request->laboratory_id],['type','laboratory']])
    ->get();

    $data = [];
    foreach ($test_laboratories as $key => $laboratory) {
        $data[$key]['id']           = (int)$laboratory->test->id;
        $data[$key]['name']         = $laboratory->test->name ?? '';
        $data[$key]['description']  = $laboratory->test->description ?? '';
        $data[$key]['recommend']    = $laboratory->test->recommend ?? '';
        $data[$key]['amount']       = (int)$laboratory->test->fee ?? 0;
    }

    return result($data, $this->successStatus, 'All Test Laboratory Wise');
  }

  public function testCategory(Request $request)
  {
    $request->validate([
      'category_id' => 'required|exists:laboratories,id'
    ]);
    $test_category=TestCategory::with('test')->orderBy('created_at','desc')
    ->where([['foreign_id',$request->category_id],['type','category']])
    ->get();
    
    $data = [];
    foreach ($test_category as $key => $category) {
        $data[$key]['id']           = (int)$category->test->id;
        $data[$key]['name']         = $category->test->name ?? '';
        $data[$key]['description']  = $category->test->description ?? '';
        $data[$key]['recommend']    = $category->test->recommend ?? '';
        $data[$key]['amount']       = (int)$category->test->fee ?? 0;
    }
    return result($data, $this->successStatus, 'All Test Category Wise');
  }

  public function testFeatured(Request $request)
  {
    $featured_test=LaboratoryTest::orderBy('created_at','desc')
    ->where('is_featured',1)
    ->get();
    return result($featured_test, $this->successStatus, 'All Featured Test');
  }

  public function testFeaturedRecent(Request $request)
  {
    $featured_test=LaboratoryTest::orderBy('created_at','desc')
    ->where('is_featured',1)
    ->take(5)
    ->get();
    return result($featured_test, $this->successStatus, 'All Featured Test');
  }

  public function testDetail(Request $request)
  {
    $request->validate([
      'test_id' =>  'required|exists:laboratory_tests,id'
    ]);
    
    $tests = LaboratoryTest::with('laboratory.test','laboratory.laboratory','banners')->where([['id',$request->test_id]])->first();
    $data['test']['id']               = (int)$tests->id;
    $data['test']['name']             = $tests->name ?? '';
    $data['test']['image']            = $tests->image ?? '';
    $data['test']['description']      = $tests->description ?? '';
    
    $data['banners']      = [];
    $data['laboratory']   = [];
    
    foreach ($tests->laboratory as $key => $laboratory) {
      $data['laboratory'][$key]['id']           = (int)$laboratory->laboratory->id;
      $data['laboratory'][$key]['name']         = $laboratory->laboratory->name ?? '';
      $data['laboratory'][$key]['amount']       = (int)$laboratory->test->fee ?? 0;
    }
    foreach ($tests->banners as $key => $banner) {
      $data['banners'][$key]['id']        = (int)$banner->id;
      $data['banners'][$key]['banner']    = $banner->banner ?? '';
    }
    return result($data, $this->successStatus, 'Test Detail');
  }
}
