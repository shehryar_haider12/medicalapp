<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Validator;
use Storage;
use Auth;
use Hash;

class UserController extends Controller
{
    public $successStatus = 200;
    public $errorStatus = 401;

    /**
     * Store Api Data of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[ 
            'name'                      =>  'required|max:255',
            'dob'                       =>  'required',
            'phone'                     =>  'required|max:11',
            'email'                     =>  'nullable|max:255|email|unique:users,email',
            'password'                  =>  'required|confirmed',
            'password_confirmation'     =>  'required', 
            'avatar'                    =>  'nullable|image|mimes:jpg,jpeg,png|max:3000',        
        ]);
   
        $input              =   $request->all(); 
        $input['dob']       =   date('Y-m-d', strtotime($input['dob']));
        $input['password']  =   Hash::make($input['password']); 
        if($request->avatar)
        {
            $input['avatar'] = Storage::disk('uploads')->putFile('',$request->avatar);
        }
        $user               =   User::create($input); 
        $success['token']   =   $user->createToken('MyApp')->accessToken; 
        $success['user']    =   $user;
        return result($success,$this->successStatus, "success");
    }

    /** 
     * login Through Api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function login(Request $request)
    { 
        // return $request->all();
        $validator = Validator::make($request->all(),[ 
            'phone'     =>  'required|max:11|exists:users,phone',
            'password'  =>  'required',
        ]);
        $user = User::where('phone',$request->phone)->first();

        if(!empty($user) && Hash::check($request->password, $user['password']))
        { 
            if($user->status == 0)
            {
                return response()->json([
                    'message' => 'user is not active.'
                ], 401);    
            }
            $device_token = $request->device_token ?? '';
            $user->update(['device_token'=>$device_token]);
            $success['token'] =  $user->createToken('MyApp')->accessToken; 
            return result($success,$this->successStatus, "success");
        } 
        else{
            return result('invalid phone or password',$this->errorStatus, "error");
         } 
    }

    /**
     * Update Api Data of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user_id    = $request->user_id ?? 0;
        $user       = User::find($user_id);
        $validator  = Validator::make($request->all(),[ 
            'user_id'                   =>  'required|exists:users,id',
            'name'                      =>  'required|max:100',
            'dob'                       =>  'required',
            'email'                     =>  'required|max:255|email|unique:users,email,'.$user->id,
            'phone'                     =>  'required|max:11|unique:users,phone,'.$user->id,
            'avatar'                    =>  'nullable|image|mimes:jpg,jpeg,png|max:3000',        
        ]);
        $input              =   $request->all(); 
        if($request->avatar)
        {
            Storage::disk('uploads')->delete($user->image);
            $input['avatar'] = Storage::disk('uploads')->putFile('',$request->avatar);
        }
        $input['dob']       =   date('Y-m-d', strtotime($input['dob']));
        $user->update($input);
        
        return result($user,$this->successStatus, "Profile Updated Successfully");
    }

    /**
     * Update Api Data of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function changePassword(Request $request)
    {
        $user_id    = $request->user_id ?? 0;
        $user       = User::find($user_id);
        $validator  = Validator::make($request->all(),[ 
            'user_id'               =>  'required|exists:users,id',
            'old_password'          =>  'required',
            'password'              =>  'required|confirmed',
            'password_confirmation' =>  'required',         
        ]);
        $input  =   $request->all(); 
        if(!Hash::check($input['old_password'], $user->password))
        {
            return result('error',$this->errorStatus, "your current password does not match");
        }
        $input['password'] = Hash::make($input['password']);
        $user->update($input);
        return result($user,$this->successStatus, "Password Changed Successfully");
    }

    /** 
     * Details of Specified Resource 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function details(Request $request) 
    { 
        $user = $request->user();
        if($user->status == 0)
        {
            return response()->json([
                'message' => 'user is not verified.'
            ], 401);
        }

        $user['avatar'] = url('')."/uploads/".$user['avatar'];
        return result($user,$this->successStatus, "User Personal Detail");
    } 
}

