<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;

class CategoryController extends Controller
{
    public $successStatus = 200;
    public $errorStatus = 401;

    public function categoryRecent()
    {
        $recent=Category::take(5)
        ->select(['id','name','image'])
        ->orderBy('created_at','desc')
        ->get();
        return result($recent, $this->successStatus, 'Popular Recent Categories');
    }

    public function categoryAll()
    {
        $all=Category::
        select(['id','name','image'])
        ->orderBy('created_at','desc')
        ->get();
        return result($all, $this->successStatus, 'All Popular Categories');
    }

    public function categoryDetail(Request $request)
    {
        $request->validate([
            'category_id'   =>  'required|exists:categories,id'
        ]);
        $category=Category::with('testCategory.test')->select(['id','name','image'])
        ->where('id',$request->category_id)
        ->first();

        $data['category']['id']               = (int)$category->id;
        $data['category']['name']             = $category->name ?? '';
        $data['category']['image']            = $category->image ?? '';
        $data['category']['description']      = $category->description ?? '';
        
        $data['banners'] = [];
        $data['tests']   = [];
        foreach ($category->testCategory as $key => $test) {
          $data['tests'][$key]['id']           = (int)$test->test->id;
          $data['tests'][$key]['name']         = $test->test->name ?? '';
          $data['tests'][$key]['description']  = $test->test->description ?? '';
          $data['tests'][$key]['recommend']    = $test->test->recommend ?? '';
          $data['tests'][$key]['amount']       = (int)$test->test->fee ?? 0;
        }
        foreach ($category->banners as $key => $banner) {
          $data['banners'][$key]['id']        = (int)$banner->id;
          $data['banners'][$key]['banner']    = $banner->banner ?? '';
        }

        return result($data, $this->successStatus, 'Category Detail');
    }
}
