<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Cart;
use App\Models\LaboratoryTest;

class CartController extends Controller
{
    public $successStatus = 200;
    public $errorStatus = 401;

    public function addCart(Request $request){
        $data = $request->validate([
            'test_id'   =>  'required|exists:laboratory_tests,id',
            'user_id'   =>  'required',
        ]);
        $user_id = $data['user_id'];
        $exist = Cart::where([['user_id',$user_id],['test_id',$data['test_id']]])->first();
        $test = LaboratoryTest::where('id',$data['test_id'])->first();
        if(!empty($exist))
        {
            $quantity = $exist->quantity + 1;
            $exist->update(['quantity'=>$quantity]);
            return response()->json([
                'message'   =>  'success',
            ]);
        }
        $data['user_id'] = $user_id;
        $data['amount']  = $test->fee;
        Cart::create($data);
        return response()->json([
            'message'   =>  'success',
        ]);
    }

    public function getCart(Request $request){
        $request->validate([
            'user_id'   =>  'required',
        ]);
        $user_id = $request->user_id;
        $carts = Cart::with('test')->where([['user_id',$user_id]])->get();
        $data['cart'] = [];

        foreach ($carts as $key => $cart) {
            $data['cart'][$key]['id']        = (int)$cart->id;
            $data['cart'][$key]['name']      = $cart->test->name ?? '';
            $data['cart'][$key]['amount']    = $cart->amount ?? 0;
        }
        return response()->json([
            'message'   =>  'success',
            'data'      =>  $data,
        ]);
    }

    public function removeCart(Request $request){
        $data = $request->validate([
            'cart_id'   =>  'required|exists:add_to_cart,id',
        ]);
        $exist = Cart::where([['id',$data['cart_id']]])->delete();
        return response()->json([
            'message'   =>  'success',
        ]);
    }
}
