<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Slider;
use Storage;
use DataTables;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('slider.index');
    }

    public function datatable()
    {
        $slider = Slider::all();
        return DataTables::of($slider)->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data=[
            'isEdit' => false
          ];
          return view('slider.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'text'          =>  'required|string|max:255',
            'heading'   =>  'required',
            'image'        =>  'required|max:3000|mimes:png,jpg,jpeg,webp',
          ]);
          $data['image']      = Storage::disk('uploads')->putFile('',$request->image);
          Slider::create($data);
          return redirect()->route('slider.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Slider $slider)
    {
        $data=[
            'isEdit' => true,
            'slider' => $slider
          ];
          return view('slider.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Slider $slider)
    {
        $data =   $request->validate([
            'text'          =>  'required|string|max:255',
            'heading'   =>  'required',
            'image'        =>  'nullable|max:3000|mimes:png,jpg,jpeg,webp',
          ]);
          if($request->image)
          {
            Storage::disk('uploads')->delete($slider->image);
            $data['image'] = Storage::disk('uploads')->putFile('',$request->image);
          }
          $slider->update($data);
          return redirect()->route('slider.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $slider = Slider::findOrFail($request->id);
        // apply your conditional check here
        if ( $slider->delete()) {
            $response['success'] = 'This Slider Deleted Successfully';
            return response()->json($response, 200);
        } else {
            $response['error'] = 'Oops Something went wrong!';
            return response()->json($response, 409);
        }
    }
}
