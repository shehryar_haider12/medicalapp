<?php
use App\Models\Message;
use App\Models\Notification;
use App\Models\Transaction;
use App\Models\Membership;
use App\Models\LoginHistory;

 function result($data, $status, $message)
 {
    return response()->json([
        "data"      =>  $data,
        "code"      =>  $status,
        "message"   =>  $message,
        "image_url" =>  url('')."/uploads/",
    ], $status);
 }

/**
 * Get Specified Site Setting
 * @return array 
 */
//  function getSiteSetting($key)
//  {
//     $setting =  Setting::where('key',$key)->first();
//     return $setting->value;
//  }

//  /**
//  * Get Specified Site Setting
//  * @return array 
//  */
// function loginHistory($data)
// {
//    $data['datetime'] = now();
//    LoginHistory::create($data);  
// }