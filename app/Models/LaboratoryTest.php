<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;

class LaboratoryTest extends Model
{
    use SoftDeletes;

    protected $table = 'laboratory_tests';
    protected $fillable = ['laboratory_id','image','name','description','is_featured','created_by','updated_by'];

    public static function boot()
    {
       parent::boot();
       static::creating(function($model)
       {
           $user = Auth::user();
           $model->created_by = $user->id;
           $model->updated_by = $user->id;
       });
       static::updating(function($model)
       {
           $user = Auth::user();
           $model->updated_by = $user->id;
       });
    }

    public function laboratory()
    {
        return $this->hasMany('App\Models\TestCategory', 'test_id', 'id')->where('type','laboratory');
    }

    public function category()
    {
        return $this->hasMany('App\Models\TestCategoryDetail', 'test_id', 'id');
    }

    public function banners()
    {
        return $this->hasMany('App\Models\Banner', 'foreign_id', 'id')->where('type','test');
    }
}
