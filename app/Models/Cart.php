<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\LaboratoryTest;

class Cart extends Model
{
    protected $table = 'add_to_cart';
    protected $fillable = ['id','amount','user_id','quantity','test_id'];

    /**
     * Get the test associated with the Cart
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function test()
    {
        return $this->hasOne(LaboratoryTest::class, 'id', 'test_id');
    }
}
