<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class FeaturedServices extends Model
{
    protected $table='featured_services';
    protected $fillable=[
      'name',
      'image'
    ];

    public static function boot()
    {
       parent::boot();
       static::creating(function($model)
       {
           $user = Auth::user();
           $model->created_by = $user->id;
           $model->updated_by = $user->id;
       });
       static::updating(function($model)
       {
           $user = Auth::user();
           $model->updated_by = $user->id;
       });
    }
}
