<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;
use App\Models\TestCategory;
use App\Models\Banner;

class Category extends Model
{
    use SoftDeletes;
    
    protected $table = 'categories';
    protected $fillable = ['image','name','created_by','updated_by','status'];

     /**
     * Get all of the comments for the Laboratory
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function testCategory()
    {
        return $this->hasMany(TestCategoryDetail::class, 'category_id', 'id');
    }

    /**
     * Get all of the banners for the Laboratory
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function banners()
    {
        return $this->hasMany(Banner::class, 'foreign_id', 'id')->where('type','category');
    }

    public static function boot()
    {
       parent::boot();
       static::creating(function($model)
       {
           $user = Auth::user();
           $model->created_by = $user->id;
           $model->updated_by = $user->id;
       });
       static::updating(function($model)
       {
           $user = Auth::user();
           $model->updated_by = $user->id;
       });
    }
}
