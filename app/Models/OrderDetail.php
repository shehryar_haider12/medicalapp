<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\TestCategory;
use App\Models\Laboratory;
use App\User;

class OrderDetail extends Model
{
    protected $table = 'order_details';
    protected $fillable = ['order_no','user_id','test_id','laboratory_id','amount'];

     /**
     * Get the test associated with the Cart
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function test_category()
    {
        return $this->hasOne(TestCategory::class, 'id', 'test_id');
    }

    /**
     * Get the test associated with the Cart
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function laboratory()
    {
        return $this->hasOne(Laboratory::class, 'id', 'laboratory_id');
    }

    /**
     * Get the test associated with the Cart
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne(user::class, 'id', 'user_id');
    }
}
