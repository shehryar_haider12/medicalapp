<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\OrderDetail;

class Order extends Model
{
    protected $table = 'order';
    protected $fillable = ['order_no','total_amount'];

     /**
     * Get the test associated with the Cart
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function order_detail()
    {
        return $this->hasMany(OrderDetail::class, 'order_no', 'order_no');
    }
}
