<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IncludeTest extends Model
{
    protected $table='include_tests';
    protected $fillable=[
      'lab_test_id',
      'test_id',
    ];
}
