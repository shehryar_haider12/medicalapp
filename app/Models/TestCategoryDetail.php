<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TestCategoryDetail extends Model
{
    protected $table = 'test_categories_detail';
    protected $fillable = ['test_id','category_id'];

    /**
     * Get the test associated with the TestCategory
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function test()
    {
        return $this->hasOne(LaboratoryTest::class, 'id', 'test_id');
    }
}
