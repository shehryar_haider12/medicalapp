<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Notification;
use Auth;

class memberShipExpiredReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'membership:expired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This will remind a member or facility about the expiration of membership';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user = transaction();
        $notification = new Notification;
        $from = Auth::user()->select(['first_name','last_name','avatar'])->first();
        $message = getSiteSetting('membership_expired_reminder');
        $notification->toMultiDevice($user, $from,$message,null,'FLUTTER_NOTIFICATION_CLICK');
        echo "Command Schedule Successfully!";

    }
}
