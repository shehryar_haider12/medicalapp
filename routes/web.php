<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return redirect('admin/login');
});
Route::group(['prefix' => 'admin'], function () {

// Route::Dashboard
Route::group(['prefix' => 'dashboard'], function () {
    
    Route::get('/', 'HomeController@dashboard')->name('dashboard');
    Route::get('/settings', 'HomeController@settings')->name('users.settings');
    Route::patch('/settings', 'HomeController@patch')->name('users.settings');
});

// Route::Laboratories
Route::group(['prefix' => 'laboratories'], function () {

    Route::get('','LaboratoryController@index')->name('laboratory');
    Route::get('/datatable','LaboratoryController@datatable')->name('laboratory.datatable');
    Route::get('/create','LaboratoryController@create')->name('laboratory.create');
    Route::post('/store','LaboratoryController@store')->name('laboratory.store');
    Route::get('/edit/{laboratory}','LaboratoryController@edit')->name('laboratory.edit');
    Route::put('/update/{laboratory}','LaboratoryController@update')->name('laboratory.update');
    Route::patch('/status','LaboratoryController@status')->name('laboratory.status');
    Route::delete('/delete','LaboratoryController@destroy')->name('laboratory.delete');
});

// Route:: Test
Route::group(['prefix' => '/test'], function () {

    Route::get('','TestController@index')->name('test');
    Route::get('/datatable','TestController@datatable')->name('test.datatable');
    Route::get('/create','TestController@create')->name('test.create');
    Route::post('/store','TestController@store')->name('test.store');
    Route::get('/edit/{test}','TestController@edit')->name('test.edit');
    Route::put('/update/{test}','TestController@update')->name('test.update');
    Route::patch('/status','TestController@status')->name('test.status');
    Route::delete('/delete','TestController@destroy')->name('test.delete');
});

// Route::Laboratory Test
Route::group(['prefix' => 'laboratory/test'], function () {

    Route::get('','LaboratoryTestController@index')->name('laboratory-test');
    Route::get('/datatable','LaboratoryTestController@datatable')->name('laboratory-test.datatable');
    Route::get('/create','LaboratoryTestController@create')->name('laboratory-test.create');
    Route::post('/store','LaboratoryTestController@store')->name('laboratory-test.store');
    Route::get('/edit/{laboratory_test}','LaboratoryTestController@edit')->name('laboratory-test.edit');
    Route::put('/update/{laboratory_test}','LaboratoryTestController@update')->name('laboratory-test.update');
    Route::patch('/status','LaboratoryTestController@status')->name('laboratory-test.status');
    Route::delete('/delete','LaboratoryTestController@destroy')->name('laboratory-test.delete');
});

// Route::Laboratory Test
Route::group(['prefix' => 'category'], function () {

    Route::get('','CategoryController@index')->name('category');
    Route::get('/datatable','CategoryController@datatable')->name('category.datatable');
    Route::get('/create','CategoryController@create')->name('category.create');
    Route::post('/store','CategoryController@store')->name('category.store');
    Route::get('/edit/{category}','CategoryController@edit')->name('category.edit');
    Route::put('/update/{category}','CategoryController@update')->name('category.update');
    Route::patch('/status','CategoryController@status')->name('category.status');
    Route::delete('/delete','CategoryController@destroy')->name('category.delete');
});

// Route::Laboratory Test
Route::group(['prefix' => 'advertisement'], function () {

    Route::get('','AdvertisementController@index')->name('advertisement.index');
    Route::get('/datatable','AdvertisementController@datatable')->name('advertisement.datatable');
    Route::get('/create','AdvertisementController@create')->name('advertisement.create');
    Route::post('/store','AdvertisementController@store')->name('advertisement.store');
    Route::get('/edit/{advertisement}','AdvertisementController@edit')->name('advertisement.edit');
    Route::put('/update/{advertisement}','AdvertisementController@update')->name('advertisement.update');
    Route::patch('/status','AdvertisementController@status')->name('advertisement.status');
    Route::delete('/delete','AdvertisementController@destroy')->name('advertisement.delete');
});

Route::group(['prefix' => 'slider'], function () {

    Route::get('','SliderController@index')->name('slider.index');
    Route::get('/datatable','SliderController@datatable')->name('slider.datatable');
    Route::get('/create','SliderController@create')->name('slider.create');
    Route::post('/store','SliderController@store')->name('slider.store');
    Route::get('/edit/{slider}','SliderController@edit')->name('slider.edit');
    Route::put('/update/{slider}','SliderController@update')->name('slider.update');
    // Route::patch('/status','SliderController@status')->name('slider.status');
    Route::delete('/delete','SliderController@destroy')->name('slider.delete');
});

Route::group(['prefix' => 'services'], function () {

    Route::get('','FeaturedServicesController@index')->name('services.index');
    Route::get('/datatable','FeaturedServicesController@datatable')->name('services.datatable');
    Route::get('/create','FeaturedServicesController@create')->name('services.create');
    Route::post('/store','FeaturedServicesController@store')->name('services.store');
    Route::get('/edit/{services}','FeaturedServicesController@edit')->name('services.edit');
    Route::put('/update/{services}','FeaturedServicesController@update')->name('services.update');
    // Route::patch('/status','FeaturedServicesController@status')->name('services.status');
    Route::delete('/delete','FeaturedServicesController@destroy')->name('services.delete');
});

Route::group(['prefix' => 'enrollment'], function () {

    Route::get('','EnrollmentController@index')->name('enrollment.index');
    Route::get('/datatable','EnrollmentController@datatable')->name('enrollment.datatable');
    
});
Route::get('banner/{banner}/delete','HomeController@bannerDelete')->name('banner_image.delete');
Auth::routes();
});
// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

// Cache Cleared Route
Route::get('exzed/cache', function() {
    Artisan::call('cache:clear');
    Artisan::call('view:clear');
    Artisan::call('route:clear');
    Artisan::call('config:clear');
   return 'cache clear';
});


