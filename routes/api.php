<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//---------------------------- User Routes

// Route::group(['prefix' => 'memberships'], function () {
//     Route::get('/all','Api\SiteController@getAll');
// });


// Route::any('{url?}/{sub_url?}', function () {
//     return result(false, 404, "Route not Found.");
// });

Route::prefix('user')->group(function () {
    
    Route::post('/register', 'Api\UserController@store');
    Route::post('/login', 'Api\UserController@login');
    // Apply Middle ware on user authentication
    Route::group(['middleware' => 'auth:api'], function(){
        Route::get('details', 'Api\UserController@details');
    });
    Route::post('/update/{userId}', 'Api\UserController@update');
    Route::post('/change-password/{userId}', 'Api\UserController@changePassword');

});
Route::group(['prefix' => 'laboratories'], function () {
  Route::get('/recent','Api\LaboratoryController@recentlabs');
  Route::get('/all','Api\LaboratoryController@allLabs');
  Route::get('/detail','Api\LaboratoryController@labDetail');
});

Route::group(['prefix' => 'test'], function () {
  Route::get('/laboratory','Api\LaboratoryController@testLaboratory');
  Route::get('/category','Api\LaboratoryController@testCategory');
  Route::get('/featured','Api\LaboratoryController@testFeatured');
  Route::get('/featured/recent','Api\LaboratoryController@testFeaturedRecent');
  Route::get('/detail','Api\LaboratoryController@testDetail');
});

Route::group(['prefix' => 'advertisement'], function () {
  Route::get('/all','Api\AdvertisementController@Banners');
});

Route::group(['prefix' => 'category'], function () {
  Route::get('/recent','Api\CategoryController@categoryRecent');
  Route::get('/all','Api\CategoryController@categoryAll');
  Route::get('/detail','Api\CategoryController@categoryDetail');
});

Route::prefix('cart')->group(function () {
  Route::post('/add','Api\CartController@addCart');
  Route::get('/get','Api\CartController@getCart');
  Route::post('/remove','Api\CartController@removeCart');
  // Route::get('/update-quantity','Api\CartController@updateQuantityCart');    
});

