/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

// For Scroll
import Vue from 'vue'
import VueChatScroll from 'vue-chat-scroll'
Vue.use(VueChatScroll)

// For Notification Toaster 
import Toaster from 'v-toaster'
// For Timeout
Vue.use(Toaster, {timeout: 5000})

import 'v-toaster/dist/v-toaster.css'

Vue.component('main-chat', require('./components/MainChatComponent.vue').default);
Vue.component('chat-list', require('./components/ChatList.vue').default);
Vue.component('message-send', require('./components/MessageSend.vue').default);
Vue.component('message-composer', require('./components/MessageComposer.vue').default);
Vue.component('message-list', require('./components/MessageList.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});
