@extends('layouts.master')

@section('title', 'Update Profile')

@section('content')
<div class="container-fluid">
    @section('breadcrumb','Update Profile')

    {{-- <button onclick="window.history.back(1)" type="button" class="btn btn-block btn-primary btn-md col-md-2 float-sm-right">Go Back</button> --}}
    <form action="{{route('users.settings')}}" method="post" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="_method" value="patch">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <i class="fa fa-users"> </i> Update Profile
                <button type="submit" class="btn btn-block btn-primary btn-md col-md-2 float-sm-right">Update</button>
            </div>
            <div class="panel-body">
                <div class="card-box">
                    <div class="row">
                        <div class="col-md-12">
                            <img src="{{url('').'/uploads/'.$user->avatar}}" alt="" style="max-width:150px">
                            <div class="form-group">
                                <label>Avatar <small>only 150x150 images accepted!</small></label>
                                <input type="file" name="avatar" parsley-trigger="change" placeholder="Name..."
                                    class="form-control" id="avatar">
                                <span class="text-danger">{{ $errors->first('avatar') ?? null }}</span>
                            </div>
                            <span class="text-danger">{{$errors->first('avatar') ?? null}}</span>
                        </div>
                        <div class="row col-md-12">
                            <div class="form-group col-md-4">
                                <label>First Name <span class="text-danger">*</span></label>
                                <input type="text" name="first_name" parsley-trigger="change" required placeholder="First Name..."
                                    class="form-control" id="irst_name" value="{{$user->first_name}}">
                            </div>
                            <span class="text-danger">{{ $errors->first('first_name') ?? null }}</span>
                            <div class="form-group col-md-4">
                                <label>Last Name <span class="text-danger">*</span></label>
                                <input type="text" name="last_name" parsley-trigger="change" required placeholder="Last Name..."
                                    class="form-control" id="last_name" value="{{$user->last_name}}">
                            </div>
                            <span class="text-danger">{{ $errors->first('last_name') ?? null }}</span>
                            <div class="form-group col-md-4">
                                <label>Email <span class="text-danger">*</span></label>
                                <input type="email" name="email" parsley-trigger="change" required="" placeholder="Email..." class="form-control" value="{{$user->email}}">
                            </div>
                            <span class="text-danger">{{ $errors->first('email') ?? null }}</span>
                            <div class="form-group">
                                <div class="mt30">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="changePasswordCheckbox" name="change_password" {{ old('change_password') == 1 ? 'checked' : null }} value="1">
                                        <label class="custom-control-label" for="changePasswordCheckbox">Change Password</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Current Password <span class="text-danger">*</span></label>
                                <input type="password" name="current_password" placeholder="Password..." class="form-control" disabled data-parsley-maxlength="22" data-parsley-minlength="6">
                            </div>
                            <span class="text-danger">{{ $errors->first('current_password') ?? null }}</span>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label>New Password <span class="text-danger">*</span></label>
                                <input type="password" name="password" placeholder="Password..." class="form-control" disabled data-parsley-maxlength="22" data-parsley-minlength="6">
                            </div>
                            <span class="text-danger">{{ $errors->first('password') ?? null }}</span>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Confirm Password <span class="text-danger">*</span></label>
                                <input type="password" name="password_confirmation" placeholder="Confirm Password..." class="form-control" disabled data-parsley-maxlength="22" data-parsley-minlength="6">
                            </div>
                            <span class="text-danger">{{ $errors->first('password_confirmation') ?? null }}</span>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </form>
</div><!-- container -->
@endsection

@section('custom-script')
<script>
    $(document).ready(function () {

        $('#changePasswordCheckbox').click(function () {
            if ($(this).prop('checked')) {
                $('input[name="current_password"]').removeAttr('disabled');
                $('input[name="current_password"]').attr('required', true);
                $('input[name="password"]').removeAttr('disabled');
                $('input[name="password"]').attr('required', true);
                $('input[name="password_confirmation"]').removeAttr('disabled');
                $('input[name="password_confirmation"]').attr('required', true);
            } else {
                $('input[name="current_password"]').removeAttr('required');
                $('input[name="current_password"]').attr('disabled', true);
                $('input[name="password"]').removeAttr('required');
                $('input[name="password"]').attr('disabled', true);
                $('input[name="password_confirmation"]').removeAttr('required');
                $('input[name="password_confirmation"]').attr('disabled', true);
            }
        });

        if ($('#changePasswordCheckbox').prop('checked')) {
            $('input[name="current_password"]').removeAttr('disabled');
            $('input[name="current_password"]').attr('required', true);
            $('input[name="password"]').removeAttr('disabled');
            $('input[name="password"]').attr('required', true);
            $('input[name="password_confirmation"]').removeAttr('disabled');
            $('input[name="password_confirmation"]').attr('required', true);
        } else {
            $('input[name="current_password"]').removeAttr('required');
            $('input[name="current_password"]').attr('disabled', true);
            $('input[name="password"]').removeAttr('required');
            $('input[name="password"]').attr('disabled', true);
            $('input[name="password_confirmation"]').removeAttr('required');
            $('input[name="password_confirmation"]').attr('disabled', true);
        }
    });

</script>
@endsection
