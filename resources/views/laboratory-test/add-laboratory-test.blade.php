@extends('layouts.master')

@section('title', 'Add Laboratory Test')

@section('top-styles')
 <!-- DataTables -->
 <link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
 <link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
 <link rel="stylesheet" href="{{url('')}}/assets/plugins/select2/css/select2.min.css">
 <style>
   .mt30 {
    margin-top: 35px;
    margin-left: 20px;
}
 </style>
 @endsection

@section('content')
    @section('breadcrumb','Add Laboratory Test')
    {{-- {{dd($laboratory_test->laboratory)}} --}}
    <div class="container-fluid">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- jquery validation -->
              <div class="card">
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <i class="fa fa-users"> </i> Add Laboratory Test
                              <button onclick="window.history.back(1)" type="button" class="btn btn-block btn-primary btn-md col-md-2 float-sm-right">Go Back</button>
                        </div>
                        <div class="panel-body">
                          <!-- form start -->
                          <form action="{{$isEdit ? route('laboratory-test.update',$laboratory_test->id) : route('laboratory-test.store')}}" role="form" method="POST" id="quickForm" enctype="multipart/form-data">
                            @csrf
                            @if ($isEdit)
                              <input type="hidden" name="_method" value="put">
                            @endif
                            <input type="hidden" name="user_type" value="1">
                            <div class="row">
                              
                              <div class="form-group col-md-4">
                                <label for="name">Select Laboratory</label>
                                <select name="laboratory_id" class="form-control"> 
                                  <option value="" selected disabled>Select Laboratory</option>
                                  @foreach ($laboratories as $laboratory)
                                      <option value="{{$laboratory->id}}" 
                                      @if ($isEdit)
                                          {{$laboratory_test->laboratory->id == $laboratory->id ? 'selected' : null}}
                                      @endif>{{$laboratory->name}} </option>
                                    @endforeach
                                </select>
                                <span class="text-danger">{{$errors->first('laboratory_id') ?? null}}</span>
                              </div>
                              <div class="form-group col-md-4">
                                <label for="name">Select Test</label>
                                <select name="test_id" class="form-control"> 
                                  <option value="" selected disabled>Select Test</option>
                                  @foreach ($tests as $test)
                                      <option value="{{$test->id}}" 
                                        @if ($isEdit)
                                          {{$laboratory_test->test_id == $test->id ? 'selected' : null}}
                                        @endif
                                        >{{$test->name}} </option>
                                    @endforeach
                                </select>
                                <span class="text-danger">{{$errors->first('test_id') ?? null}}</span>
                              </div>
                              <div class="form-group col-md-4">
                                <label for="fee">Test Fee</label>
                                <input type="text" name="fee" class="form-control" id="fee" placeholder="Enter Test Fee" value="{{$laboratory_test->fee ?? old('fee')}}">
                                <span class="text-danger">{{$errors->first('fee') ?? null}}</span>

                              </div>
                              <div class="form-group col-md-4">
                                <label for="report_in_days">Report In Days</label>
                                <input type="report_in_days" name="report_in_days" class="form-control" id="report_in_days" placeholder="Enter Report In Days" value="{{$laboratory_test->report_in_days ?? old('report_in_days')}}">
                                <span class="text-danger">{{$errors->first('report_in_days') ?? null}}</span>

                              </div>
                              <div class="form-group col-md-4">
                                <label for="recommend">Recommended For</label>
                                <input type="recommend" name="recommend" class="form-control" id="recommend" placeholder="Ex: Male, Female" value="{{$laboratory_test->recommend ?? old('recommend')}}">
                                <span class="text-danger">{{$errors->first('recommend') ?? null}}</span>

                              </div>
                                <div class="form-group col-md-4">
                                  <label for="name">Select Included Test</label>
                                  <select name="include_test_id[]" multiple class="form-control select2"> 
                                    {{-- <option value="" selected disabled>Select Facility</option> --}}
                                    @foreach ($tests as $test)
                                        <option value="{{$test->id}}"
                                          {{-- {{dd($test)}}  --}}
                                          @if($isEdit && !empty($laboratory_test->includeTest)) 
                                          {{-- {{dd($test->includeTest)}} --}}
                                          
                                          @foreach ($laboratory_test->includeTest as $include_test)
                                          {{$include_test->test_id == $test->id ? 'selected' : null}}
                                            @endforeach
                                          @endif
                                          >{{$test->name}} </option>
                                      @endforeach
                                  </select>
                                  <span class="text-danger">{{$errors->first('test_id') ?? null}}</span>
                                </div>
                              {{-- <div class="form-group col-md-4">
                                <label for="banner">Banners</label>
                                <input type="file" name="banner[]" multiple class="form-control" id="banner" accept="jpg,jpeg,png,webp">
                                <span class="text-danger">{{$errors->first('banner') ?? null}}</span>

                              </div> --}}
                            </div>
                              {{-- <div class="form-group col-2">
                                <label for="avail">Avail (per day)</label>
                                <input type="avail" name="avail" class="form-control" id="avail" placeholder="Enter Avail" value="{{$laboratory->avail ?? old('avail')}}">
                                <span class="text-danger">{{$errors->first('avail') ?? null}}</span>

                              </div>
                              <div class="form-group col-3">
                                <label for="avail">Total Days of Membership</label>
                                <input type="avail" name="days" class="form-control" id="days" placeholder="Enter days" value="{{$laboratory->days ?? old('days')}}">
                                <span class="text-danger">{{$errors->first('days') ?? null}}</span>

                              </div>
                              <div class="form-group col-12">
                                <label for="avail">Agreement Page Link</label>
                                <input type="avail" name="agreement_link" class="form-control" placeholder="Enter Page Link" value="{{$laboratory->agreement_link ?? old('agreement_link')}}">
                                <span class="text-danger">{{$errors->first('agreement_link') ?? null}}</span>

                              </div> --}}
                            {{-- </div> --}}
                              <div class="card-footer">
                                  <button type="submit" class="btn btn-primary">Submit</button>
                              </div>
                          </form>
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
              </div>
            <!--/.col (left) -->
            <!-- right column -->
            <div class="col-md-6">
  
            </div>
            <!--/.col (right) -->
          </div>
        
    </div>
  <!-- ./wrapper -->
@section('page-scripts')
<!-- jquery-validation -->
<script src="{{url('')}}/assets/plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="{{url('')}}/assets/plugins/jquery-validation/additional-methods.min.js"></script>
<script src="{{url('')}}/assets/plugins/select2/js/select2.min.js"></script>
@endsection
@section('custom-script')
<script type="text/javascript">
    $(document).ready(function () {
      $('.select2').select2();

      $('#quickForm').validate({
        rules: {
          name: { required: true, },
          address: { required: true, },
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
        }
      });

      function readURL(input, number) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#imagePreview' + number).css('background-image', 'url(' + e.target.result + ')');
                    $('#imagePreview' + number).hide();
                    $('#imagePreview' + number).fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#imageUpload1").change(function () {
            readURL(this, 1);
        });
    });
    </script>
@endsection    
@endsection