@extends('layouts.master')

@section('title', 'Enrollments ')

@section('top-styles')
<!-- DataTables -->
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

@endsection

@section('content')
@section('breadcrumb','Enrollments ')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <i class="fa fa-users"> </i> Enrollments 
                        </div>
                        <div class="panel-body">
                            <table id="DataTable" class="table table-bordered table-hover table-responsive" width="100%">
                                <thead>
                                    <tr>
                                        <th class="no-sort text-center" width="5%">S.No</th>
                                        <th>Application No</th>
                                        <th>User Name</th>
                                        <th>User Phone</th>
                                        <th>Laboratory</th>
                                        <th>Test</th>
                                        <th>Amount</th>
                                        <th>Application Date</th>
                                        {{-- <th>Opening Days</th>
                                        <th>Open At</th>
                                        <th>Close At</th> --}}
                                        <th class="no-sort text-center" width="12%">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
    </div>
</div>
<!-- ./wrapper -->
@section('page-scripts')
<!-- DataTables -->
<script src="{{url('')}}/assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{url('')}}/assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="{{url('')}}/assets/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="{{url('')}}/assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>

@endsection
@section('custom-script')
<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#DataTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{route("enrollment.datatable")}}',
            "columns": [{
                    "data": "id",
                    "defaultContent": ""
                },
                {
                    "data": "order_no",
                    "defaultContent": ""
                },
                {
                    "data": "user_id",
                    "defaultContent": ""
                },
                {
                    "data": "user_id",
                    "defaultContent": ""
                },
                {
                    "data": "laboratory_id",
                    "defaultContent": ""
                },
                {
                    "data": "test_id",
                    "defaultContent": ""
                },
                {
                    "data": "amount",
                    "defaultContent": ""
                },
                {
                    "data": "created_at",
                    "defaultContent": ""
                },
               
                {
                    "data": "id",
                    "defaultContent": ""
                },
            ],
            "columnDefs": [{
                    "targets": 'no-sort',
                    "orderable": false,
                },
                {
                    "targets": 0,
                    "render": function (data, type, row, meta) {
                        return meta.row + 1;
                    },
                },
                {
                    "targets": 2,
                    "render": function (data, type, row, meta) {
                        return row.user.name;
                    },
                },
                {
                    "targets": 3,
                    "render": function (data, type, row, meta) {
                        return row.user.phone;
                    },
                },
                {
                    "targets": 4,
                    "render": function (data, type, row, meta) {
                        return row.laboratory.name;
                    },
                },
                {
                    "targets": 5,
                    "render": function (data, type, row, meta) {
                        return row.test_category.test.name;
                    },
                },
                {
                    "targets": -1,
                    "render": function (data, type, row, meta) {
                        var edit = '{{route("category.edit",[":id"])}}';
                        edit = edit.replace(':id', data);
                        var checked = row.status == 1 ? 'checked' : null;
                        return `
                        <a href="` + edit + `" class="text-info p-1">
                            <i class="fa fa-check"></i> Confirm
                        </a>
                        `;
                    },
                },
            ],
            "drawCallback": function (settings) {
                var elems = Array.prototype.slice.call(document.querySelectorAll('.status'));
                if (elems) {
                elems.forEach(function (html) {
                    var switchery = new Switchery(html, {
                    color: '#007bff'
                    , secondaryColor: '#dfdfdf'
                    , jackColor: '#fff'
                    , jackSecondaryColor: null
                    , className: 'switchery'
                    , disabled: false
                    , disabledOpacity: 0.5
                    , speed: '0.1s'
                    , size: 'small'
                    });

                });
                }
            },
      //scrollX:true,
        });
    });

</script>
@endsection
@endsection
