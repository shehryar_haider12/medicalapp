<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="A Content Management System designed by abc.com">
  <meta name="author" content="abc.com">

  <link rel="shortcut icon" href="{{url('')}}/assets/custom/images/favicon3.ico">

  <title>Login - Rehmat-e-Sheeren</title>

  <link href="{{url('')}}/assets/custom/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="{{url('')}}/assets/custom/css/icons.css" rel="stylesheet" type="text/css" />
  <link href="{{url('')}}/assets/custom/css/style.css" rel="stylesheet" type="text/css" />

  <link href="{{url('')}}/assets/custom/css/animate.min.css" rel="stylesheet" type="text/css" />
  <link href="{{url('')}}/assets/custom/css/custom.css" rel="stylesheet" type="text/css" />

  <script src="{{url('')}}/assets/custom/js/modernizr.min.js"></script>

</head>

<body>

  <div class="account-pages bg-dark-theme">
    <div id="canvas-wrapper">
      <canvas id="demo-canvas"></canvas>
    </div>
  </div>
  <div class="clearfix"></div>

  <div class="login_page_custom">
    <div class="wrapper-page login_custom_con">
      <div class="animated fadeInDown">
        <div class="text-center bg-white b_r_5 bb_l_r_0 p10">
          <h2>Rehmat-e-Sheeren</h2>
        </div>
        <div class="card-box bg-black-transparent4 bt_l_r_0">
          <div class="panel-heading text-center">

            <h1 class="text-white m0">
              <strong> Verify Code </strong>
            </h1>
          </div>

          <div class="p-20">
            <form class="form-horizontal" action="{{route('verify')}}" method="post" autocomplete="off">
              @csrf
              @if ($errors->any())
              <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                  ×
                </button>
                @foreach ($errors->all() as $item)
                {{ $item }}
                <br>
                @endforeach
              </div>
              @else
              <div class="alert alert-info alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                  ×
                </button>
                A
                <b>Code</b> has been sent to your email.
              </div>
              @endif
              <div class="form-group m-b-0">
                <div class="input-group">
                  <input type="number" class="form-control" placeholder="Enter Code" required="" name="code">
                  <span class="input-group-append">
                    <button type="submit" class="btn btn-light-theme w-sm waves-effect waves-light">
                      Verify
                    </button>
                  </span>
                </div>
              </div>

            </form>

          </div>
        </div>
      </div>
    </div>
  </div>




  <script>
    var resizefunc = [];
  </script>

  <!-- jQuery  -->
  <script src="{{url('')}}/assets/custom/js/jquery.min.js"></script>
  <script src="{{url('')}}/assets/custom/js/popper.min.js"></script>
  <!-- Popper for Bootstrap -->
  <script src="{{url('')}}/assets/custom/js/bootstrap.min.js"></script>
  <script src="{{url('')}}/assets/custom/js/detect.js"></script>
  <script src="{{url('')}}/assets/custom/js/fastclick.js"></script>
  <script src="{{url('')}}/assets/custom/js/jquery.slimscroll.js"></script>
  <script src="{{url('')}}/assets/custom/js/jquery.blockUI.js"></script>
  <script src="{{url('')}}/assets/custom/js/waves.js"></script>
  <script src="{{url('')}}/assets/custom/js/wow.min.js"></script>
  <script src="{{url('')}}/assets/custom/js/jquery.nicescroll.js"></script>
  <script src="{{url('')}}/assets/custom/js/jquery.scrollTo.min.js"></script>

  <!-- lOGIN Page Plugins -->
  <script src="{{url('')}}/assets/custom/pages/login/EasePack.min.js"></script>
  <script src="{{url('')}}/assets/custom/pages/login/rAF.js"></script>
  <script src="{{url('')}}/assets/custom/pages/login/TweenLite.min.js"></script>
  <script src="{{url('')}}/assets/custom/pages/login/login.js"></script>


  <script src="{{url('')}}/assets/custom/js/jquery.core.js"></script>
  <script src="{{url('')}}/assets/custom/js/jquery.app.js"></script>

  <script type="text/javascript">
    jQuery(document).ready(function () {

      // Init CanvasBG and pass target starting location
      CanvasBG.init({
        Loc: {
          x: window.innerWidth / 2,
          y: window.innerHeight / 3.3
        },
      });

    });
  </script>

</body>

</html>
