@extends('layouts.master')

@section('title', 'Add Test')

@section('top-styles')
 <!-- DataTables -->
 <link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
 <link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
 <link rel="stylesheet" href="{{url('')}}/assets/plugins/select2/css/select2.min.css">
 <style>
   .mt30 {
    margin-top: 35px;
    margin-left: 20px;
  }
   .images {
        height: 150px;
        margin: 11px;
    }
    span.fa.fa-close {
        float: right;
        color: #f05050d6;
        font-size: 20px;
        position: relative;
        bottom: 0px;
        left: 0px;
    }
 </style>
 @endsection

@section('content')
    @section('breadcrumb','Add Test')
    {{-- {{dd($test->laboratory)}} --}}
    <div class="container-fluid">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- jquery validation -->
              <div class="card">
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <i class="fa fa-users"> </i> Add Test
                              <button onclick="window.history.back(1)" type="button" class="btn btn-block btn-primary btn-md col-md-2 float-sm-right">Go Back</button>
                        </div>
                        <div class="panel-body">
                          <!-- form start -->
                          <form action="{{$isEdit ? route('test.update',$test->id) : route('test.store')}}" role="form" method="POST" id="quickForm" enctype="multipart/form-data">
                            @csrf
                            @if ($isEdit)
                              <input type="hidden" name="_method" value="put">
                            @endif
                            <div class="row">
                              <div class="col-md-4">
                                <div class="avatar-upload">
                                    <div class="avatar-edit">
                                        <input type='file' name="image" id="imageUpload1"
                                            accept=".png, .jpg, .jpeg" />
                                        <label for="imageUpload1"><span>Featured Image  <span class="text-danger">*</span></span></label>
                                    </div>
                                    <div class="avatar-preview">
                                        <div id="imagePreview1"
                                            style="background-image : url({{url('').'/uploads/'}}{{$test->image ??'placeholder.jpg'}}">
                                        </div>
                                    </div>
                                </div>
                                <span class="text-danger">{{$errors->first('image') ?? null}}</span>
                              </div>
                              <div class="col-md-7">
                              <div class="form-group">
                                <label for="name">Select Category</label>
                                <select name="category_id[]" multiple class="form-control select2"> 
                                  {{-- <option value="" selected disabled>Select Facility</option> --}}
                                  @foreach ($categories as $category)
                                      <option value="{{$category->id}}" 
                                        @if($isEdit) 
                                        {{-- {{dd($test->category)}} --}}
                                        @foreach ($test->category as $cat)
                                        {{$cat->category_id == $category->id ? 'selected' : null}}
                                          @endforeach
                                        @endif
                                        >{{$category->name}} </option>
                                    @endforeach
                                </select>
                                <span class="text-danger">{{$errors->first('category_id') ?? null}}</span>
                              </div>
                              <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" name="name" class="form-control" id="name" placeholder="Enter Test Name" value="{{$test->name ?? old('name')}}">
                                <span class="text-danger">{{$errors->first('name') ?? null}}</span>

                              </div>
                              <div class="form-group">
                                <label for="banner">Banners</label>
                                <input type="file" name="banner[]" multiple class="form-control" id="banner" accept="jpg,jpeg,png,webp">
                                <span class="text-danger">{{$errors->first('banner') ?? null}}</span>

                              </div>
                            </div>
                            <div class="form-group col-md-7">
                              <label for="description">Description</label>
                              <input type="description" name="description" class="form-control" id="description" placeholder="Enter Description" value="{{$test->description ?? old('description')}}">
                              <span class="text-danger">{{$errors->first('description') ?? null}}</span>

                            </div>
                              <div class="mt30">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="is_featured" name="is_featured" @if($isEdit) {{ $test->is_featured == 1 ? 'checked' : null }}@endif value="1">
                                    <label class="custom-control-label" for="is_featured">Featured</label>
                                </div>
                            </div>
                            </div>
                            <div class="section row">
                              @if ($isEdit)
                                  @foreach ($test->banners as $item)
                                      <div class="images col-md-2">
                                          <a href="{{route('banner_image.delete',$item->id)}}"><span class="fa fa-close fa-window-close"></span></a>
                                          <img src="{{url('').'/uploads/'.$item->banner}}" height='121px' width='190px'>
                                      </div>
                                  @endforeach
                              @endif
                            </div>
                              {{-- <div class="form-group col-2">
                                <label for="avail">Avail (per day)</label>
                                <input type="avail" name="avail" class="form-control" id="avail" placeholder="Enter Avail" value="{{$laboratory->avail ?? old('avail')}}">
                                <span class="text-danger">{{$errors->first('avail') ?? null}}</span>

                              </div>
                              <div class="form-group col-3">
                                <label for="avail">Total Days of Membership</label>
                                <input type="avail" name="days" class="form-control" id="days" placeholder="Enter days" value="{{$laboratory->days ?? old('days')}}">
                                <span class="text-danger">{{$errors->first('days') ?? null}}</span>

                              </div>
                              <div class="form-group col-12">
                                <label for="avail">Agreement Page Link</label>
                                <input type="avail" name="agreement_link" class="form-control" placeholder="Enter Page Link" value="{{$laboratory->agreement_link ?? old('agreement_link')}}">
                                <span class="text-danger">{{$errors->first('agreement_link') ?? null}}</span>

                              </div> --}}
                            {{-- </div> --}}
                              <div class="card-footer">
                                  <button type="submit" class="btn btn-primary">Submit</button>
                              </div>
                          </form>
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
              </div>
            <!--/.col (left) -->
            <!-- right column -->
            <div class="col-md-6">
  
            </div>
            <!--/.col (right) -->
          </div>
        
    </div>
  <!-- ./wrapper -->
@section('page-scripts')
<!-- jquery-validation -->
<script src="{{url('')}}/assets/plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="{{url('')}}/assets/plugins/jquery-validation/additional-methods.min.js"></script>
<script src="{{url('')}}/assets/plugins/select2/js/select2.min.js"></script>
@endsection
@section('custom-script')
<script type="text/javascript">
    $(document).ready(function () {
      $('.select2').select2();

      $('#quickForm').validate({
        rules: {
          name: { required: true, },
          address: { required: true, },
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
        }
      });

      function readURL(input, number) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#imagePreview' + number).css('background-image', 'url(' + e.target.result + ')');
                    $('#imagePreview' + number).hide();
                    $('#imagePreview' + number).fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#imageUpload1").change(function () {
            readURL(this, 1);
        });
    });
    </script>
@endsection    
@endsection