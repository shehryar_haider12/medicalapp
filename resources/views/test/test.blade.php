@extends('layouts.master')

@section('title', 'Tests')

@section('top-styles')
<!-- DataTables -->
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

@endsection

@section('content')
@section('breadcrumb','Tests')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <i class="fa fa-users"> </i> Tests
                            <a href="{{route('test.create')}}" class="col-md-2 float-sm-right">
                                <button type="button" class="btn btn-block btn-primary btn-md">Add Test</button>
                            </a>
                        </div>
                        <div class="panel-body">
                            <table id="DataTable" class="table table-bordered table-hover table-responsive" width="100%">
                                <thead>
                                    <tr>
                                        <th class="no-sort text-center" width="5%">S.No</th>
                                        <th>Image</th>
                                        <th>Test Name</th>
                                        <th>Featured</th>
                                        <th class="no-sort text-center" width="7%">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
    </div>
</div>
<!-- ./wrapper -->
@section('page-scripts')
<!-- DataTables -->
<script src="{{url('')}}/assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{url('')}}/assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="{{url('')}}/assets/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="{{url('')}}/assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>

@endsection
@section('custom-script')
<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#DataTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{route("test.datatable")}}',
            "columns": [{
                    "data": "id",
                    "defaultContent": ""
                },
                {
                    "data": "image",
                    "defaultContent": ""
                },
                {
                    "data": "name",
                    "defaultContent": ""
                },
                {
                    "data": "is_featured",
                    "defaultContent": ""
                },
                {
                    "data": "id",
                    "defaultContent": ""
                },
            ],
            "columnDefs": [{
                    "targets": 'no-sort',
                    "orderable": false,
                },
                {
                    "targets": 0,
                    "render": function (data, type, row, meta) {
                        return meta.row + 1;
                    },
                },
                {
                    "targets": 1,
                    "render": function (data, type, row, meta) {
                        return `<img src='{{url('')}}/uploads/` + data +
                            `' height='50px' alt='image'/>`;
                    },
                },
                {
                    "targets": 3,
                    "render": function (data, type, row, meta) {
                        var featured  = data == 1 ? 'Yes' : 'No';
                        return featured;
                    },
                },
                {
                    "targets": -1,
                    "render": function (data, type, row, meta) {
                        var edit = '{{route("test.edit",[":id"])}}';
                        edit = edit.replace(':id', data);
                        var checked = row.status == 1 ? 'checked' : null;
                        return `
                        <a href="` + edit + `" class="text-info p-1">
                            <i class="fa fa-edit"></i>
                        </a>
                        <a href="javascript:;" class="delete text-danger p-2" data-id="` + row.id + `">
                            <i class="fa fa-trash"></i>
                        </a>
                        `;
                    },
                },
            ],
            "drawCallback": function (settings) {
                var elems = Array.prototype.slice.call(document.querySelectorAll('.status'));
                if (elems) {
                elems.forEach(function (html) {
                    var switchery = new Switchery(html, {
                    color: '#007bff'
                    , secondaryColor: '#dfdfdf'
                    , jackColor: '#fff'
                    , jackSecondaryColor: null
                    , className: 'switchery'
                    , disabled: false
                    , disabledOpacity: 0.5
                    , speed: '0.1s'
                    , size: 'small'
                    });

                });
                }

                $('.status').change(function () {
                var $this = $(this);
                var id = $this.val();
                var status = this.checked;

                if (status) {
                    status = 1;
                } else {
                    status = 0;
                }

                axios
                    .post('{{route("test.status")}}', {
                    _token: '{{csrf_token()}}',
                    _method: 'patch',
                    id: id,
                    status: status,
                    })
                    .then(function (responsive) {
                    console.log(responsive);
                    })
                    .catch(function (error) {
                    console.log(error);
                    });
                });
                
                $('.delete').click(function () {
                var deleteId = $(this).data('id');
                var $this = $(this);

                swal({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#4fa7f3',
                    cancelButtonColor: '#d57171',
                    confirmButtonText: 'Yes, delete it!'
                }).then(function (result) {
                    if (result) {
                    axios
                    .post('{{route("test.delete")}}', {
                        _method: 'delete',
                        _token: '{{csrf_token()}}',
                        id: deleteId,
                    })
                    .then(function (response) {
                        console.log(response);

                        swal(
                        'Deleted!',
                        'Facility has been deleted.',
                        'success'
                        )

                        table
                        .row($this.parents('tr'))
                        .remove()
                        .draw();
                    })
                    .catch(function (error) {
                        console.log(error);
                        swal(
                        'Failed!',
                        error.response.data.error,
                        'error'
                        )
                    });
                    }
                })
                });
            },
      //scrollX:true,
        });
    });

</script>
@endsection
@endsection
