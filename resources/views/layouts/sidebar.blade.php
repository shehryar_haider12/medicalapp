<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{route('dashboard')}}" class="brand-link">
        <img src="{{url('')}}/assets/dist/img/AdminLTELogo.png" alt="AdminLTE Logo"
            class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">Medical App</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{url('')}}/uploads/{{Auth::user()->avatar ?? 'dummy-user.jpeg'}}" class="img-circle elevation-2"
                    alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">{{Auth::user()->name ?? null}}</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
               <li class="nav-item">
                    <a href="{{route('dashboard')}}" class="nav-link {{Route::currentRouteName() == 'dashboard' ? 'active' : null}}">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('users.settings')}}" class="nav-link {{Route::currentRouteName() == 'users.settings' ? 'active' : null}}">
                        <i class="nav-icon fas fa-cogs mr-2"></i>
                        <p>Update Profile</p>
                    </a>
                </li>
                <li class="nav-item has-treeview">
                    <a href="" class="nav-link {{Route::currentRouteName() == 'laboratory' || Route::currentRouteName() == 'laboratory.create' ? 'active' : null}}">
                        <i class="nav-icon fas fa-flask mr-2"></i>
                        <p>
                            Laboratories
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('laboratory')}}" class="nav-link {{Route::currentRouteName() == 'laboratory' ? 'active' : null}}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>View Laboratory</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('laboratory.create')}}" class="nav-link {{Route::currentRouteName() == 'laboratory.create' ? 'active' : null}}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Add Laboratory</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview">
                    <a href="" class="nav-link {{Route::currentRouteName() == 'category' || Route::currentRouteName() == 'category.create' ? 'active' : null}}">
                        <i class="nav-icon fa fa-sitemap mr-2"></i>
                        <p>
                            Categories
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('category')}}" class="nav-link {{Route::currentRouteName() == 'category' ? 'active' : null}}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>View Category</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('category.create')}}" class="nav-link {{Route::currentRouteName() == 'category.create' ? 'active' : null}}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Add Category</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview">
                    <a href="" class="nav-link {{Route::currentRouteName() == 'test' || Route::currentRouteName() == 'test.create' ? 'active' : null}}">
                        <i class="nav-icon fas fa-flask mr-2"></i>
                        <p>
                             Test
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('test')}}" class="nav-link {{Route::currentRouteName() == 'test' ? 'active' : null}}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>View Test</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('test.create')}}" class="nav-link {{Route::currentRouteName() == 'test.create' ? 'active' : null}}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Add Test</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview">
                    <a href="" class="nav-link {{Route::currentRouteName() == 'laboratory-test' || Route::currentRouteName() == 'laboratory-test.create' ? 'active' : null}}">
                        <i class="nav-icon fas fa-flask mr-2"></i>
                        <p>
                            Laboratory Test
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('laboratory-test')}}" class="nav-link {{Route::currentRouteName() == 'laboratory-test' ? 'active' : null}}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>View Laboratory Test</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('laboratory-test.create')}}" class="nav-link {{Route::currentRouteName() == 'laboratory-test.create' ? 'active' : null}}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Add Laboratory Test</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview">
                    <a href="" class="nav-link {{Route::currentRouteName() == 'advertisement.index' || Route::currentRouteName() == 'advertisement.create' ? 'active' : null}}">
                        <i class="nav-icon fa fa-window-maximize mr-2"></i>
                        <p>
                            Advertisement
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('advertisement.index')}}" class="nav-link {{Route::currentRouteName() == 'advertisement.index' ? 'active' : null}}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>View Advertisement</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('advertisement.create')}}" class="nav-link {{Route::currentRouteName() == 'advertisement.create' ? 'active' : null}}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Add Advertisement</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item has-treeview">
                    <a href="" class="nav-link {{Route::currentRouteName() == 'slider.index' || Route::currentRouteName() == 'slider.create' ? 'active' : null}}">
                        <i class="nav-icon fa fa-window-maximize mr-2"></i>
                        <p>
                            Slider
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('slider.index')}}" class="nav-link {{Route::currentRouteName() == 'slider.index' ? 'active' : null}}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>View Sliders</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('slider.create')}}" class="nav-link {{Route::currentRouteName() == 'slider.create' ? 'active' : null}}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Add Slider</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item has-treeview">
                    <a href="" class="nav-link {{Route::currentRouteName() == 'services.index' || Route::currentRouteName() == 'services.create' ? 'active' : null}}">
                        <i class="nav-icon fa fa-window-maximize mr-2"></i>
                        <p>
                            Featured Services
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('services.index')}}" class="nav-link {{Route::currentRouteName() == 'services.index' ? 'active' : null}}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>View Featured Services</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('services.create')}}" class="nav-link {{Route::currentRouteName() == 'services.create' ? 'active' : null}}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Add Featured Service</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview">
                    <a href="" class="nav-link {{Route::currentRouteName() == 'enrollment.index' || Route::currentRouteName() == 'enrollment.index' ? 'active' : null}}">
                        <i class="nav-icon fa fa-window-maximize mr-2"></i>
                        <p>
                            Enrollment
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('enrollment.index')}}" class="nav-link {{Route::currentRouteName() == 'enrollment.index' ? 'active' : null}}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>View Enrollment</p>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
