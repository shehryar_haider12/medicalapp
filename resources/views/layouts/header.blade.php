@php
    // $count_messages = count(unreadMessage());
    // $messages = unreadMessage()->take(3);
    // $count_notification = count(unreadNotification());
    // if($count_notification > 0)
    // {
    //     $notifications = unreadNotification()->take(3);
    // }
  
//   dd($count_notification);
@endphp
  <!-- Navbar -->
 <nav class="main-header navbar navbar-expand navbar-white navbar-light">
     <!-- Left navbar links -->
     <ul class="navbar-nav">
         <li class="nav-item">
             <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
         </li>
         <li class="nav-item d-none d-sm-inline-block">
             <a href="{{url('/')}}" class="nav-link">Home</a>
         </li>
     </ul>

     <!-- Right navbar links -->
     <ul class="navbar-nav ml-auto">
         <!-- Notifications Dropdown Menu -->
         <li class="nav-item dropdown">
            <a class="nav-link" id="read-notification" data-user="{{Auth::user()->id}}" data-toggle="dropdown" href="#">
                 <i class="far fa-bell" style="margin-right: 8px; font-size: 17px !important;"></i>
                 <span class="badge badge-warning navbar-badge number-alert">{{0}}</span>
             </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            {{-- <span class="data-notify"> --}}
                    <span class="dropdown-item dropdown-header unread_mesg">0 unread notifications</span>
                    <div class="dropdown-divider"></div>
                    <span class="data-notify">
                       
                    </span>
                <a href="" class="dropdown-item dropdown-footer">See All Notifications</a>
            </div>
        </li>
         <li class="nav-item dropdown">
             <a class="nav-link" data-toggle="dropdown" href="#">
                 <i class="fas fa-user"></i>
             </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <span class="dropdown-item dropdown-header"><strong>Welcome
                        {{Auth::user()->name ?? null}}!</strong></span>
                <div class="dropdown-divider"></div>
                <a href="{{route('users.settings')}}" class="dropdown-item">
                    <i class="fas fa-cogs mr-2"></i> Profile
                </a>
                <a href="{{route('logout')}}" class="dropdown-item notify-item"
                    onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <i class="fa fa-sign-out-alt mr-2"></i>
                    <span>Logout</span>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </a>
            
            </div>
         </li>
     </ul>
 </nav>
 
 <!-- /.navbar -->
